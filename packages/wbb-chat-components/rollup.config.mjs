import json from "@rollup/plugin-json"
import terser from "@rollup/plugin-terser"
import resolve from '@rollup/plugin-node-resolve';
import dts from "rollup-plugin-dts";
import typescript from "@rollup/plugin-typescript";
import commonjs from "@rollup/plugin-commonjs";

// import babel from '@rollup/plugin-babel';

// const packageJson = require("./package.json");
// const dts = require('rollup-plugin-dts');

export default [
    {
    input: './src/index.ts',
    output: [
        {
            file: "./dist/cjs/index.js",
            format: 'cjs',
            sourcemap: true
        },
        {
            file: './dist/esm/index.js',
            format: "esm",
            sourcemap: true
        },
        // {
        //     file: packageJson.types,
        //     format: "esm",
        //     // sourcemap: true,
        // },
    ],
    plugins: [
        json(),
        resolve(),
        commonjs(),
        typescript(),
        terser(),
    ],
    external: ["styled-components"]
},
{
    // input: "dist/esm/types/index.d.ts",
    input: "dist/esm/index.d.ts",
    output: [{ file: "./dist/esm/index.d.ts", format: "esm" }],
    plugins: [dts()],
},
]