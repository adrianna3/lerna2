import './App.css';
import  {MyCounter} from "chat-components";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MyCounter value={9}/>
      </header>
    </div>
  );
}

export default App;
